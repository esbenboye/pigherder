#!/usr/local/bin/python3
class Catcher:
    def __init__(self, xPos, yPos):
        self.sign_on = "X"
        self.sign_off = "x"
        self.xPos = xPos
        self.yPos = yPos

    def moveUp(self, board):
        return self.move(board, 0, 1)

    def moveDown(self, board):
        return self.move(board, 0, -1)

    def moveLeft(self, board):
        return self.move(board, -1, 0)

    def moveRight(self, board):
        return self.move(board, 1, 0)

    def move(self, board, x, y):
        if(board.tileIsAvailable(self.xPos + x, self.yPos + y)):
            self.xPos = self.xPos + x
            self.yPos = self.yPos + y
            return True
        return False


