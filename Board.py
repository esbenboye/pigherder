#!/usr/bin/python3
from Pig import Pig
from Catcher import Catcher
from Obstacle import Obstacle
from random import randint
import curses
class Board:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.currentCatcherIndex = 0
        self.maxPigs = 5
        self.maxObstacles = 100
        self.maxCatchers = 20

        self.pigs = [] 
        self.catchers = []
        self.obstacles = []

        offset = 3
        placedCatchers = 0
        while placedCatchers < self.maxCatchers:
            self.catchers.append(Catcher(7, placedCatchers + offset))
            placedCatchers += 1
            
        placedPigs = 0
        while placedPigs < self.maxPigs:
            x = randint(0,self.height)
            y = randint(0, self.height)
            if self.tileIsAvailable(x,y):
                self.pigs.append(Pig(x,y))
                placedPigs += 1

        placedObstacles = 0
        while placedObstacles < self.maxObstacles:
            x = randint(0, self.height)
            y = randint(0, self.width)
            if self.tileIsAvailable(x,y):
                self.obstacles.append(Obstacle(x,y))
                placedObstacles += 1

        # Building the borders
        for x in range(0, self.width):
            self.obstacles.append(Obstacle(0, x))
            self.obstacles.append(Obstacle(self.height, x))
        for y in range(0, self.height):
            self.obstacles.append(Obstacle(y, 0))
            self.obstacles.append(Obstacle(y, self.width))

    def nextCatcher(self):
        self.currentCatcherIndex = (self.currentCatcherIndex + 1) % len(self.catchers)

    def getCurrentCatcher(self):
        return self.catchers[self.currentCatcherIndex]

    def moveCatcher(self, x, y):
        catcher = self.getCurrentCatcher()
        if self.tileIsAvailable(catcher.xPos + x, catcher.yPos + y):
            self.catchers[self.currentCatcherIndex].yPos += y
            self.catchers[self.currentCatcherIndex].xPos += x
            return True;
        return False

    def movePigs(self):
        for pig in self.pigs:
            pig.move(self)

    def tileIsAvailable(self, x, y):
        for pig in self.pigs:
            if(pig.xPos == x and pig.yPos == y):
                return False

        for catcher in self.catchers:
            if(catcher.xPos == x and catcher.yPos == y):
                return False

        for obstacle in self.obstacles:
            if(obstacle.xPos == x and obstacle.yPos == y):
                return False
        return True

    def draw(self, stdscr):
        stdscr.attron(curses.color_pair(1))
        for obstacle in self.obstacles:
            stdscr.addstr(obstacle.xPos, obstacle.yPos, obstacle.sign)

        stdscr.attron(curses.color_pair(2))
        for pig in self.pigs:
            stdscr.addstr(pig.xPos, pig.yPos, pig.sign)
            
        stdscr.attron(curses.color_pair(3))
        for catcher in self.catchers:
            stdscr.addstr(catcher.xPos, catcher.yPos, catcher.sign_off)

        stdscr.attron(curses.color_pair(4))
        currentCatcher = self.getCurrentCatcher()
        stdscr.addstr(currentCatcher.xPos, currentCatcher.yPos, currentCatcher.sign_on)
        stdscr.move(0,0)
