#!/usr/local/bin/python3
from random import shuffle
class Pig:
    def __init__(self, xPos, yPos):
        self.sign = "o"
        self.xPos = xPos
        self.yPos = yPos

    def move(self, board):
        directions = [1,2,3,4]
        shuffle(directions)
       
        # 1 = up
        # 2 = right
        # 3 = down
        # 4 = left
        while(len(directions) > 0):
            direction = directions.pop()
            if(direction == 1 and board.tileIsAvailable(self.xPos, self.yPos + 1)):
                self.yPos = self.yPos + 1
                return True
        
            if(direction == 2 and board.tileIsAvailable(self.xPos + 1, self.yPos)):
                self.xPos = self.xPos + 1
                return True

            if(direction == 3 and board.tileIsAvailable(self.xPos, self.yPos - 1)):
                self.yPos = self.yPos - 1
                return True

            if(direction == 4 and board.tileIsAvailable(self.xPos - 1,  self.yPos)):
                self.xPos = self.xPos - 1
                return True

        return False


