#! /usr/bin/python3
from Board import Board
from Pig import Pig
import sys, os
import curses
import time
import threading

board = Board(40, 20)

def move_pigs(stdscr):
    while True:
        stdscr.clear() 
        stdscr.attron(2)
        for pig in board.pigs:
            pig.move(board)
        board.draw(stdscr)
        stdscr.refresh()
        time.sleep(1)

def run_game(stdscr):
    key = 0
    curses.start_color()

    curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(4, curses.COLOR_RED, curses.COLOR_BLACK)

    t = threading.Thread(target=move_pigs, args=(stdscr,))
    t.start()
    
    while(key != ord('q')):
        stdscr.clear()

        if key == curses.KEY_UP:
            board.moveCatcher(-1,0)
        if key == curses.KEY_DOWN:
            board.moveCatcher(1,0)
        if key == curses.KEY_LEFT:
            board.moveCatcher(0,-1)
        if key == curses.KEY_RIGHT:
            board.moveCatcher(0,1)

        if key == ord('n'):
            board.nextCatcher()

        board.draw(stdscr)

        stdscr.refresh()
        key = stdscr.getch()

    t.join()

if __name__ == "__main__":
    curses.wrapper(run_game)
